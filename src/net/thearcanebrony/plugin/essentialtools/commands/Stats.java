package net.thearcanebrony.plugin.essentialtools.commands;

import java.lang.management.ManagementFactory;
import com.sun.management.OperatingSystemMXBean;

import net.thearcanebrony.plugin.essentialtools.CommandHandler;
import net.thearcanebrony.plugin.essentialtools.Lag;
import net.thearcanebrony.plugin.essentialtools.Main;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class Stats extends CommandHandler implements Listener {
	public Stats(Main plugin) {
		super(plugin);
	}

	int cpusamples = 100; 
	
	
	DecimalFormat df = new DecimalFormat("00.00");
	BossBar mem = Bukkit.createBossBar("Memory...", BarColor.BLUE, BarStyle.SEGMENTED_20);
	BossBar tps = Bukkit.createBossBar("TPS...", BarColor.PINK, BarStyle.SEGMENTED_20);
	BossBar ent = Bukkit.createBossBar("Entities...", BarColor.PINK, BarStyle.SEGMENTED_20);
	BossBar cpu = Bukkit.createBossBar("CPU...", BarColor.GREEN, BarStyle.SEGMENTED_20);
	OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
            OperatingSystemMXBean.class);
	Double entities, cmem, ctps;
	Long MemUsed, MemMax, MemAlloc;
	int items = 0, xporbs = 0, players = 0, minecarts = 0, peaceful = 0, hostile = 0,
			villager = 0, others = 0, fallingblock = 0;
	public List<BossBar> bossbars = new ArrayList<BossBar>(Arrays.asList(mem,cpu, tps,ent));
	ArrayList<Double> proccpu = new ArrayList<Double>(), syscpu = new ArrayList<Double>();
	public void onEnable() {

		df.setRoundingMode(RoundingMode.CEILING);
		MemMax = Runtime.getRuntime().maxMemory() / 1024;
		new BukkitRunnable() {

			@Override
			public void run() {
				MemUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				MemAlloc = Runtime.getRuntime().totalMemory() / 1048576;
				cmem = (MemUsed.doubleValue() / MemMax.doubleValue()) / 1024;
				ctps = Lag.getTPS();
				proccpu.add(osBean.getProcessCpuLoad()*100);
				syscpu.add(osBean.getSystemCpuLoad()*100);
				if(proccpu.size()>cpusamples) {
					proccpu.remove(0);
				}
				if(syscpu.size()>cpusamples) {
					syscpu.remove(0);
				}
				mem.setProgress(cmem);
				mem.setTitle(ChatColor.BLUE + "Memory: " + (MemUsed / 1048576) + " MB/" + MemMax / 1024 + " MB ("+MemAlloc+" MB allocated)");
				tps.setTitle(ChatColor.LIGHT_PURPLE + "TPS: " + df.format(ctps));
				cpu.setTitle(ChatColor.GREEN + "Process CPU: " + df.format(calculateAverage(proccpu)) + "% | System CPU: " + df.format(calculateAverage(syscpu))+"%");
				tps.setColor(BarColor.PINK);
				if(ctps > 21) tps.setColor(BarColor.YELLOW);
				if (ctps > 20) {
					ctps = 20D;
					tps.setColor(BarColor.PURPLE);
				}
				else if (ctps < 18) {
					tps.setColor(BarColor.YELLOW);
					if (ctps < 15)
						tps.setColor(BarColor.RED);
				}
				tps.setProgress((Double) ctps / 20);

				Double _syscpu = calculateAverage(syscpu);
				if(_syscpu >= 90) cpu.setColor(BarColor.RED);
				else if (ctps >= 75) cpu.setColor(BarColor.YELLOW);
				else cpu.setColor(BarColor.GREEN);
				cpu.setProgress(calculateAverage(syscpu)/100);
				items = xporbs = players = minecarts = peaceful = hostile = villager = others = fallingblock = 0;
				for (World w : Bukkit.getServer().getWorlds())
					for (Entity e : w.getEntities()) {
						switch (e.getType()) {
						case DROPPED_ITEM:
							items++;
							break;
						case PLAYER:
							players++;
							break;
						case EXPERIENCE_ORB:
							xporbs++;
							break;
						case ZOMBIE:
						case SKELETON:
						case CREEPER:
						case BLAZE:
						case CAVE_SPIDER:
						case SPIDER:
						case ENDERMAN:
						case EVOKER:
						case GUARDIAN:
						case WITCH:
						case WITHER:
						case VEX:
						case STRAY:
						case HUSK:
						case ILLUSIONER:
						case SILVERFISH:
						case SLIME:
						case VINDICATOR:
						case ZOMBIE_VILLAGER:
						case GIANT:
							hostile++;
							break;
						case MINECART:
						case MINECART_CHEST:
						case MINECART_COMMAND:
						case MINECART_FURNACE:
						case MINECART_HOPPER:
						case MINECART_MOB_SPAWNER:
						case MINECART_TNT:
							minecarts++;
							break;
						case BAT:
						case CHICKEN:
						case COW:
						case SHEEP:
						case DONKEY:
						case HORSE:
						case IRON_GOLEM:
						case LLAMA:
						case MULE:
						case SQUID:
						case MUSHROOM_COW:
						case OCELOT:
						case SNOWMAN:
						case RABBIT:
						case PARROT:
						case WOLF:
						case PIG:
						case CAT:
							peaceful++;
							break;
						case VILLAGER:
							villager++;
							break;
						case FALLING_BLOCK:
							fallingblock++;
							break;
						case PRIMED_TNT:
						case ARROW:
						case SNOWBALL:
						case SPLASH_POTION:
						case ENDER_PEARL:
						case AREA_EFFECT_CLOUD:
						case LLAMA_SPIT:
						case BOAT:
						case PAINTING:
						case ITEM_FRAME:
						case ARMOR_STAND:
						case FIREWORK:
							others++;
							break;
						default:
							System.out.println(e.getName());
							others++;
						}

					}

				entities = (items+xporbs+players+minecarts+peaceful+hostile+villager+fallingblock+others)+0d;
				ent.setTitle("Entities: " + Math.round(entities) + " (Item: " + items + " XP: " + xporbs
						+ " Player: " + players + " Cart: " + minecarts + " Mob: " + peaceful
						+ " Enemy: " + hostile + " Villager: " + villager + " FB: " + fallingblock + " Other: " + others + ")");
				if (entities > 5000)
					entities = 5000D;

				ent.setProgress(entities / 5000);

			}
		}.runTaskTimer(plugin, 0L, 1L);
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName());
		if (args.length == 0) {
			if (!sender.hasPermission("thearcanetest.stats")) {
				sender.sendMessage(ChatColor.RED + "You do not have permission to get server stats");
				return true;
			}
			if(mem.getPlayers().contains(p)) {
				cpu.removePlayer(p);
				mem.removePlayer(p);
				tps.removePlayer(p);
				ent.removePlayer(p);
			}
			else {
				cpu.addPlayer(p);
				mem.addPlayer(p);
				tps.addPlayer(p);
				ent.addPlayer(p);	
			}
			return true;
		}
		return false;
	}
	private double calculateAverage(List <Double> list) {
		  Double sum = 0d;
		  if(!list.isEmpty()) {
		    for (Double num : list) {
		        sum += num;
		    }
		    return sum.doubleValue() / list.size();
		  }
		  return sum;
		}

}
