package net.thearcanebrony.plugin.essentialtools.portals;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import net.thearcanebrony.plugin.essentialtools.Main;

public class Warp {
	public Warp(Player p, String warpName) {
		System.out.println("1 " + p.getName() + " " + warpName);
		String worldName = Main.getPlugin().getConfig().getString("Warps." + warpName + ".world");
		System.out.println("2 Name: " + worldName);
		World world = Bukkit.getWorld(worldName);
		System.out.println("3");
		double x = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".x");
		double y = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".y");
		double z = (double) Main.getPlugin().getConfig().getDouble("Warps." + warpName + ".z");
		float yaw = (float) Main.getPlugin().getConfig().getLong("Warps." + warpName + ".yaw");
		float pitch = (float) Main.getPlugin().getConfig().getLong("Warps." + warpName + ".pitch");

		System.out.println("4 " + x + "/" + y + "/ " + z + " | " + yaw + "/" + pitch);
		p.getPlayer().teleport(new Location(world, x, y, z, yaw, pitch));
	}
}