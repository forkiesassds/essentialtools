package net.thearcanebrony.plugin.essentialtools.worldedit;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import net.thearcanebrony.plugin.essentialtools.CommandHandler;
import net.thearcanebrony.plugin.essentialtools.Lag;
import net.thearcanebrony.plugin.essentialtools.Main;

public class Setbelow extends CommandHandler {
	public Setbelow(Main plugin) {
		super(plugin);
	}

	Random rnd = new Random();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		Player p = (Player) sender;
		if (!sender.hasPermission("seaponyplugin.worldedit")) {
			sender.sendMessage(ChatColor.RED + "You do not have permission.");
			return true;
		}

		int i = 0;
		long startTime = System.nanoTime();

		
		if (args.length > 0 && Material.getMaterial(args[0]) == null)
			return true;
		if (args.length == 1) {
			for (int y = 0; y < p.getLocation().getBlockY(); y++) {
				p.getWorld().getBlockAt(p.getLocation().getBlockX(), y, p.getLocation().getBlockZ())
						.setType(Material.getMaterial(args[0]));
				i++;
			}
		} else if (args.length == 2) {
			final int r = Integer.parseInt(args[1]);
			final int px = p.getLocation().getBlockX(), pz = p.getLocation().getBlockZ();
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					int _i = 0;
					int _r = 0;
					
					
					BossBar task = Bukkit.createBossBar("TASKS...", BarColor.GREEN, BarStyle.SEGMENTED_20);
					task.addPlayer(p);
					ArrayList<BukkitTask> tasks = new ArrayList<BukkitTask>();
					for (int rad = _r; rad < r; rad++) {

						long _startTime = System.nanoTime();
						int _i2 = 0;
						for (int y = 0; y < p.getLocation().getBlockY(); y++) {
							for (int x = px - rad; x <= px + rad; x++) {

								for (int z = pz - rad; z <= pz + rad; z++) {
									final int _x = x, _y = y, _z = z, __i=_i;
									if (!p.getWorld().getBlockAt(_x, _y, _z).getType().name().equals(args[0])) {
										_i++;
										_i2++;
										//final BukkitTask _t;
										//p.sendMessage(p.getWorld().getBlockAt(_x, _y, _z).getType().name());
										final BukkitTask t = Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
											int curr = __i;
											//final BukkitTask _t = t;	
											
											@Override
											public void run() {
												
												p.getWorld().getBlockAt(_x, _y, _z)
														.setType(Material.getMaterial(args[0]));
												// i++;
												task.setTitle("Worldedit task: "+curr+"/"+tasks.size()+" Current block: "+_x+"/"+_y+"/"+_z);
												task.setProgress(curr/tasks.size());
												if(curr == tasks.size()) task.removePlayer(p);
												//tasks.remove(_t);
											}
										}, (long) (rnd.nextFloat() * (rad * 2 + _r * 5)) + (_y / 2));
										//_t = t;
										tasks.add(t);
										while(Lag.getTPS()<=19.8)
											try {
												Thread.sleep(100);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										// },
										// (long)(rnd.nextFloat()*(_i/500))+(_y/2)
										// ));

									}

								}
							}
							_r = rad;
						}
						p.sendMessage(ChatColor.YELLOW + "Queued " + _i2 + " blocks ("+tasks.size()+" tasks) (r=" + rad + ") in "
								+ new DecimalFormat("#.######").format(((System.nanoTime() - _startTime) / 1000000d))
								+ " ms.");
					}

					p.sendMessage(ChatColor.YELLOW + "Queued " + _i + " blocks ("+tasks.size()+" tasks)total in "
							+ new DecimalFormat("#.######").format(((System.nanoTime() - startTime) / 1000000d))
							+ " ms.");
					boolean running = true;
					while(running) {
						String oldname = task.getTitle();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(oldname.equals(task.getTitle())) task.removePlayer(p);
					}
					
				}
			});

		}

		return true;
	}

}
