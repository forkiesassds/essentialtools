package net.thearcanebrony.plugin.essentialtools;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Util {

	public Util() {
		// TODO Auto-generated constructor stub
	}
	public static boolean CheckSurroundingBlocks(Block block, Material material) {
		Location loc = block.getLocation();
		World wld = loc.getWorld();
		double x = loc.getX(), y = loc.getY(), z = loc.getZ();
		BlockAtEquals(wld, x, y, z, material);
		return BlockAtEquals(wld, x+1, y, z, material) || BlockAtEquals(wld, x-1, y, z, material)
				|| BlockAtEquals(wld, x, y+1, z, material) || BlockAtEquals(wld, x, y-1, z, material)
				|| BlockAtEquals(wld, x, y, z+1, material) || BlockAtEquals(wld, x, y+1, z, material);
	}
	public static boolean BlockAtEquals(World wld, double x, double y, double z, Material mat) {
		return BlockEquals(wld.getBlockAt((int)x, (int)y, (int)z), mat);
	}
	public static boolean BlockEquals(Block block, Material material) {
		return block.getType() == material;
	}

}
