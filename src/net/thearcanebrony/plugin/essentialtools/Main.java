package net.thearcanebrony.plugin.essentialtools;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
//import org.bukkit.craftbukkit.v1_12_R1.entity.CraftHorse;
//import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import net.milkbowl.vault.economy.Economy;
import net.thearcanebrony.plugin.essentialtools.commands.About;
import net.thearcanebrony.plugin.essentialtools.commands.Stats;
import net.thearcanebrony.plugin.essentialtools.commands.UnloadChunks;
import net.thearcanebrony.plugin.essentialtools.commands.Whois;
import net.thearcanebrony.plugin.essentialtools.datastorage.ConfigDefaults;
import net.thearcanebrony.plugin.essentialtools.worldedit.Fill;
import net.thearcanebrony.plugin.essentialtools.worldedit.Outline;
import net.thearcanebrony.plugin.essentialtools.worldedit.Setbelow;
import net.thearcanebrony.plugin.essentialtools.worldedit.WEHandler;
import net.thearcanebrony.plugin.essentialtools.worldedit.Walls;

public class Main extends JavaPlugin implements Listener {
	private Stats stats = new Stats(this);
	DecimalFormat df = new DecimalFormat("#.##");

	Economy econ = null;

	ScoreboardManager scoreboardManager = Bukkit.getScoreboardManager();
	String playerMoney = "0";
	Scoreboard board;
	Objective objective;
	Long MemUsed, MemMax;
	Double mempercent;
	long lastTimeRun = 0, lastTick = 0, timeSinceLastTick = 0;
	int entities = 0, numScores;
	public static List<Player> barrierViewers = new ArrayList<Player>();
	boolean WaterPhysicsEnabled = false;

	@Override
	public void onEnable() {
		/*
		 * for (Player p : Bukkit.getOnlinePlayers()) {
		 * p.kickPlayer("Please rejoin, otherwise you will see frozen stats"); }
		 */
		FileConfiguration cfg = getConfig();
		ConfigDefaults.setupDefaults(cfg);
		cfg.options().copyDefaults(true);
		//cfg.setDefaults(cfg);

		try {
			cfg.save("plugins/"+getPlugin().getName()+"/config.yml");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 0L, 1L);
		getCommand("whois").setExecutor(new Whois(this));
		getCommand("unloadchunks").setExecutor(new UnloadChunks(this));
		getCommand("stats").setExecutor(stats);
		//getCommand("createworld").setExecutor(new WorldManager(this));
		getCommand("about").setExecutor(new About(this));
		getCommand(".fill").setExecutor(new Fill(this));
		getCommand(".setbelow").setExecutor(new Setbelow(this));
		getCommand(".walls").setExecutor(new Walls(this));
		getCommand(".outline").setExecutor(new Outline(this));
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		stats.onEnable();
		WaterPhysicsEnabled = cfg.getBoolean("Water Physics");
		/*
		 * new BukkitRunnable() {
		 * 
		 * @Override public void run() { for (World world : Bukkit.getWorlds())
		 * { for (Entity ent : world.getEntities()) {
		 * 
		 * try {
		 * 
		 * ent.setGlowing(true); ent.setGravity(false); ((CraftCreature)
		 * ent).setTarget((LivingEntity) ent); } catch (Exception e) { } if
		 * (ent.getType() == EntityType.PLAYER) ent.setGravity(true); if
		 * (ent.getType() != EntityType.PLAYER && (ent.getType() ==
		 * EntityType.SQUID || ent.getType() == EntityType.ZOMBIE ||
		 * ent.getType() == EntityType.SKELETON || ent.getType() ==
		 * EntityType.ARROW || ent.getTicksLived() >= 10 * 20)) ent.remove(); }
		 * 
		 * } } }.runTaskTimer(getPlugin(), 0L, 20L);
		 */

		for (Player p : getServer().getOnlinePlayers())
			handlePlayerJoin(p);

		scoreboardManager = Bukkit.getScoreboardManager();
		new BukkitRunnable() {
			@Override
			public void run() {

				int len=0;
				lastTick = System.nanoTime() / 1000000;
				if (lastTick >= lastTimeRun + 1000) {
					timeSinceLastTick = lastTick - lastTimeRun;
					lastTimeRun = System.nanoTime() / 1000000;
					MemUsed = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					MemMax = Runtime.getRuntime().maxMemory() / 1024;
					mempercent = (MemUsed.doubleValue() / MemMax.doubleValue()) / 1024 * 100;
					for (Player p : getServer().getOnlinePlayers()) {
						//p.setPlayerListHeaderFooter(new TextComponent(), new TextComponent());
						len = 0;
						for(String line : getConfig().getStringList("Tablist.Header")) {
							String sline = parseVars(line, p);
							if(sline.length()>len) len = sline.length();
						}
						for(String line : getConfig().getStringList("Tablist.Footer")) {
							String sline = parseVars(line, p);
							if(sline.length()>len) len = sline.length();
						}
						try {
							p.setPlayerListHeaderFooter(parseVars(String.join("\n", getConfig().getStringList("Tablist.Header")), p, len), parseVars(String.join("\n", getConfig().getStringList("Tablist.Footer")), p, len));
							
						}
						catch (Exception e) {
							System.out.println("Failed to set player list info for player " + p.getName() + ":");
							e.printStackTrace();
						}
						try {
							
							board = scoreboardManager.getNewScoreboard();
							len=0;
							objective = board.registerNewObjective("sidebar", "dummy");
							if(!getConfig().contains("Player Data."+p.getName()+".Scoreboard Enabled") || getConfig().getBoolean("Player Data."+p.getName()+".Scoreboard Enabled")) {

								objective.setDisplaySlot(DisplaySlot.SIDEBAR);
								objective.setDisplayName(parseVars(getConfig().getString("Sidebar.Header"),p));
								//						numScores = getConfig().getInt("Sidebar.Count");
								numScores = getConfig().getStringList("Siderbar.Lines").size();
								
								for (String line : getConfig().getStringList("Sidebar.Lines")) {
									String sline = parseVars(line, p);
									if(sline.length()>len) len = sline.length();
								}
								linecounter = 0;
								//System.out.println(len);
								for (String line : getConfig().getStringList("Sidebar.Lines")) {
									String sline = parseVars(line, p, len);
									
									if(sline.length()>40) sline = sline.substring(0,40);
									//System.out.println(sline.replace("§", "&"));
									try {
										objective.getScore(sline).setScore(--numScores);
									}
									catch (Exception e) {
										System.out.println("STRING TOO LONG FOR SCOREBOARD: " + sline);
									}
								}
							}
							p.setScoreboard(board);
						}
						catch (Exception e) {
							System.out.println("Failed to set scoreboard for player " + p.getName() + ":");
							e.printStackTrace();
						}
						colorIteration++;
					}
				}
			}
		}.runTaskTimer(getPlugin(), 0L, 1L);



		new BukkitRunnable() {
			@Override
			public void run() {
				for (Player p : barrierViewers) {
					Location ploc = p.getLocation();
					int r = 16;
					for (int x = ploc.getBlockX() - r; x <= ploc.getBlockX() + r; x++) {
						for (int y = Math.max(0, ploc.getBlockY() - r); y <= Math.max(0,
								Math.min(256, ploc.getBlockY() + r)); y++) {
							for (int z = ploc.getBlockZ() - r; z <= ploc.getBlockZ() + r; z++) {
								Location bloc = new Location(p.getWorld(), x + 0.5, y + 0.5, z + 0.5);
								if (p.getWorld().getBlockAt(bloc).getType() == Material.BARRIER) {
									p.spawnParticle(Particle.BARRIER, bloc, 0);
								}
							}
						}
					}
				}
			}
		}.runTaskTimer(getPlugin(), 0L, 20L);
	
	}

    @EventHandler
    public void serverListPing(ServerListPingEvent event) {
    	
    }
    private static Field headerField;
	/*public static void setPlayerListHeader(Player player, String header, String footer) {
		CraftPlayer cplayer = (CraftPlayer) player;
		PlayerConnection connection = cplayer.getHandle().playerConnection;

		// JsonReader.setLenient(true);
		// IChatBaseComponent top = ChatSerializer.a("{\"text\": \"abc\"}");
		IChatBaseComponent tabheader = ChatSerializer.a("{\"text\": \"" + header + "\"}");
		IChatBaseComponent tabfooter = ChatSerializer.a("{\"text\": \"" + footer + "\"}");

		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
		try {
			headerField = packet.getClass().getDeclaredField("a");
			headerField.setAccessible(true);
			headerField.set(packet, tabheader);
			headerField = packet.getClass().getDeclaredField("b");
			headerField.setAccessible(true);
			headerField.set(packet, tabfooter);
			// headerField.setAccessible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		connection.sendPacket(packet);
	}*/

	public static Plugin getPlugin() {
		return Bukkit.getPluginManager().getPlugin("EssentialTools");
	}

	@Override
	public void onDisable() {
		for (BossBar bb : stats.bossbars) {
			for (Player bbp : bb.getPlayers()) {
				bb.removePlayer(bbp);
			}
		}
		for(World w : getServer().getWorlds()) {
			w.save();
			getServer().broadcastMessage(getConfig().getString("Chat Prefix") + "Saved world " + w.getName() + "!");
		}
	}

	@EventHandler
	public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent e) {
		handlePlayerJoin(e.getPlayer());
	}

	public void handlePlayerJoin(Player p) {
		p.sendMessage(getConfig().getString("Welcome Message"));
		// p.setPlayerListHeaderFooter("Welcome to AstroPlexMC, " +
		// p.getDisplayName() + "!", "Enjoy your stay!");
		/*
		 * new BukkitRunnable() { Player lp = p;
		 * 
		 * @Override public void run() { } }.runTaskTimer(getPlugin(),0L,20L);
		 */

		// if (!welcomeBar.getPlayers().contains(p))
		// welcomeBar.addPlayer(p);
		p.performCommand("stats");
		for(String cmd : getConfig().getStringList("Commands On Join")) p.performCommand(cmd);
		for(String cmd : getConfig().getStringList("Player Data."+p.getName()+".Commands On Join")) p.performCommand(cmd);
		for(String cmd : getConfig().getStringList("Console Commands On Join")) p.performCommand(cmd);
		for(String cmd : getConfig().getStringList("Player Data."+p.getName()+".Console Commands On Join")) p.performCommand(cmd);
		
		
	}	

	public static WEHandler wehandler = new WEHandler();

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerUse(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block bl = event.getClickedBlock();
			if (getConfig().getBoolean("Stair Sitting") && bl.getType().toString().contains("STAIR")) {
				Location loc = bl.getLocation();
				loc.add(0.5, -1.00, 0.5);
				Horse ent = (Horse) p.getWorld().spawnEntity(loc, EntityType.HORSE);
				ent.addPassenger(p);
				ent.setAI(false);
				/*((CraftHorse) ent).getHandle().setInvisible(true);
				((CraftHorse) ent).getHandle().setTamed(true);
				((CraftHorse) ent).getHandle().setSilent(true);
				((CraftHorse) ent).getHandle().setInvulnerable(true);*/
				// ((LivingEntity)
				// ((CraftHorse)ent).getHandle()).setCollidable(false);
				ent.setGravity(false);
				ent.setAdult();
				//ent.addPotionEffects((Collection<PotionEffect>) new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
				p.getServer().dispatchCommand(p.getServer().getConsoleSender(), "minecraft:effect @e[type=horse] minecraft:invisibility 1000000 1 true");
				//p.getServer().dispatchCommand(new CustomConsoleCommandSender(), "minecraft:effect @e[type=horse] minecraft:invisibility 1000000 1 true");
				//
				event.setCancelled(true);
			}
		}
		wehandler.onPlayerUse(event);
	}

	@EventHandler
	public void VehicleExitEvent(VehicleExitEvent e) {
		Vehicle v = e.getVehicle();
		if (v.getType() == EntityType.HORSE)
			v.remove();
	}

	@EventHandler
	public void onDraw(PlayerInteractEvent e) {
		if (e.getItem() != null && e.getItem().getType() == Material.BOW) {
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				e.getPlayer().launchProjectile(Arrow.class);
				e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.ENTITY_SKELETON_SHOOT, 1f, 1f);
				e.setCancelled(true);
			}
		}
	}
	/*
	 * @EventHandler public void onInventoryClick(InventoryClickEvent e) { if
	 * (e.getSlotType() == SlotType.CRAFTING || e.getSlotType() ==
	 * SlotType.RESULT || e.getSlotType() == SlotType.ARMOR)
	 * e.setCancelled(true); }
	 */
	/*@EventHandler
	public void craftItem(PrepareItemCraftEvent e) {

		/*
		 * Material itemType = e.getRecipe().getResult().getType(); Byte
		 * itemData = e.getRecipe().getResult().getData().getData();
		 * if(itemType==Material.ENDER_CHEST||itemType==Material.HOPPER||(
		 * itemType==Material.GOLDEN_APPLE&&itemData==1)) {
		 * e.getInventory().setResult(new ItemStack(Material.AIR));
		 * for(HumanEntity he:e.getViewers()) { if(he instanceof Player) {
		 * ((Player)he).sendMessage(ChatColor.RED+"You cannot craft this!"); } }
		 * }
		 */
	//}

	//	@EventHandler
	//	public void onblockDamage(BlockDamageEvent e) {
	//		Player p = e.getPlayer();
	//
	//	}

	Random rnd = new Random();
	String chatPrefix = getConfig().getString("Chat Prefix");
	//	public void onPlayerInteract(PlayerInteractEvent event) {
	//		Player player = event.getPlayer();
	//		if (!(event.getAction() == Action.RIGHT_CLICK_AIR)) return;
	//		if (!(event.getItem().getType() == Material.BLAZE_ROD)) return;
	//
	//		Arrow snowball = player.launchProjectile(Arrow.class);
	//		// Get vectors
	//		Vector i = snowball.getVelocity();
	//		Vector j = snowball.getLocation().toVector();
	//
	//		// Perform the subtraction, then normalize
	//		Vector r = i.subtract(j);
	//		//Vector v = r.normalize();
	//
	//		// then, actually set the speed, in this case, by a magnitude of four
	//		//Vector velocity = v;//.multiply(4);
	//
	//		// Then, set the velocity of the snowball but updating the 'default' velocity of the snowball
	//		//snowball.setVelocity(velocity);
	//		snowball.setVelocity(snowball.getVelocity().multiply(400));
	//	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName()).getPlayer();
		if (command.getName().equalsIgnoreCase("whatsmyip")) {
			sender.sendMessage(chatPrefix + "Your ip is " + p.getAddress().getHostString());

			return true;
		} else if (command.getName().equalsIgnoreCase("plugincmd")) {
			p.sendMessage(
					"/" + args[0] + " is owned by " + getServer().getPluginCommand(args[0]).getPlugin().getName());
			return true;
		} else if (command.getName().equalsIgnoreCase(".sb")) {
			WEHandler.getPlayerData(p).seeBarriers = !WEHandler.getPlayerData(p).seeBarriers;
			boolean sb = WEHandler.getPlayerData(p).seeBarriers;
			if (sb) {
				p.sendMessage(chatPrefix + "You can now see barriers!");
				barrierViewers.add(p);
			} else {
				p.sendMessage(chatPrefix + "You can no longer see barriers!");
				barrierViewers.remove(p);
			}
			return true;
		} else if (command.getName().equalsIgnoreCase("tw")) {

			WaterPhysicsEnabled = !WaterPhysicsEnabled;
			if (WaterPhysicsEnabled) {
				getServer().broadcastMessage(chatPrefix + "Water physics enabled!");
			} else {
				getServer().broadcastMessage(chatPrefix + "Water physics disabled!");
			}
			getConfig().set("Water Physics", WaterPhysicsEnabled);
			this.saveConfig();
			return true;
		} else if (command.getName().equalsIgnoreCase("togglesit")) {
			boolean sit = !getConfig().getBoolean("Stair Sitting");

			if (sit) {
				p.sendMessage(chatPrefix + "Sitting enabled!");
			} else {
				p.sendMessage(chatPrefix + "Sitting disabled!");
			}
			getConfig().set("Stair Sitting", sit);
			this.saveConfig();
			return true;
		} else if (command.getName().equalsIgnoreCase("togglescoreboard")) {
			boolean sit = !getConfig().getBoolean("Player Data."+p.getName()+".Scoreboard Enabled");

			if (sit) {
				p.sendMessage(chatPrefix + "Scoreboard enabled!");
			} else {
				p.sendMessage(chatPrefix + "Scoreboard disabled!");
			}
			getConfig().set("Player Data."+p.getName()+".Scoreboard Enabled", sit);
			this.saveConfig();
			return true;
		}
		return false;
	}
		@EventHandler(priority = EventPriority.HIGHEST)
		public void onBlockPhysics(BlockPhysicsEvent event) {
			Material mat = event.getBlock().getType();
			
			//System.out.println(getConfig().getBoolean("Classic Liquids"));
			if (getConfig().getBoolean("Classic Liquids") && (mat == Material.WATER || mat == Material.LAVA)) {
				//System.out.println("Classic liquids triggered at " + event.getBlock().getLocation());
	
				event.getBlock().setType(event.getBlock().getType());
				//event.getBlock().setBlockData(Bukkit.createBlockData("{level: 15}"));
			}
			if(getConfig().getBoolean("Obsidian Generator") && event.getChangedType() == Material.REDSTONE_WIRE) {
				System.out.println("Obsidian Gen triggered at " + event.getBlock().getLocation());
				event.getBlock().setType(Material.OBSIDIAN);
			}
			
			
			if (!getConfig().getBoolean("Grass Spreading") && (mat == Material.GRASS_BLOCK || mat == Material.DIRT)) {
				event.setCancelled(true);
				return;
			}
			/*if(event.getChangedType().name().equals(event.getSourceBlock().getType().name()))event.setCancelled(true);
			else System.out.println(event.getChangedType().name() + " -> "+event.getSourceBlock().getType().name());*/
		}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockFromTo(BlockFromToEvent event) {
		World world = event.getBlock().getWorld();
		WorldBorder wb = world.getWorldBorder();
		Location wbc = wb.getCenter();
		int wbs = (int) (wb.getSize() / 2);
		Block block = event.getToBlock(), oblock = event.getBlock();
		System.out.println(block.getType().name());
		if(getConfig().getBoolean("Obsidian Generators") && oblock.getType()==Material.WATER && block.getType() == Material.REDSTONE_WIRE) {
			if(Util.CheckSurroundingBlocks(block, Material.LAVA)) {
				System.out.println("Obsidian Gen 2 triggered at " + event.getBlock().getLocation());
				block.setType(Material.OBSIDIAN);
				event.setCancelled(true);
			}
		}
		if (!WaterPhysicsEnabled || block.getX() < wbc.getBlockX() - wbs || block.getX() > wbc.getBlockX() + wbs
				|| block.getZ() < wbc.getBlockZ() - wbs || block.getZ() > wbc.getBlockZ() + wbs)
			event.setCancelled(true);

	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public int pingPlayer(Player who) {
		try {
			String bukkitversion = Bukkit.getServer().getClass().getPackage().getName().substring(23);
			Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit." + bukkitversion + ".entity.CraftPlayer");
			Object handle = craftPlayer.getMethod("getHandle").invoke(who);
			Integer ping = (Integer) handle.getClass().getDeclaredField("ping").get(handle);
			return ping.intValue();
		} catch (Exception e) {
			return -1;
		}
	}

	int colorIteration = 0;
	ChatColor[] colors = { ChatColor.RED, ChatColor.GOLD, ChatColor.YELLOW, ChatColor.GREEN, ChatColor.BLUE,
			ChatColor.LIGHT_PURPLE, ChatColor.DARK_PURPLE };

	int linecounter = 0;
	public String parseVars(String in, Player p) {
		return parseVars(in, p, 0);
	}
	public String parseVars(String in, Player p, int length) {
		if (econ == null) {
			playerMoney = "Economy not initialised!";
			setupEconomy();
		}
		else
			playerMoney = econ.format(econ.getBalance(p));
		entities = 0;
		for (World w : getServer().getWorlds()) {
			entities += w.getEntities().size();
		}
		if(in.contains("$line")) linecounter++;
		if(linecounter > 10) linecounter = 10;
		if(length > 75) length = 75;
		return in
				.replace("$players", getServer().getOnlinePlayers().size() + "")
				.replace("$maxplayers", getServer().getMaxPlayers() + "")
				.replace("$tps", df.format(Lag.getTPS()))
				.replace("$mspt", "Not implemented")
				.replace("$mem", df.format(mempercent))
				.replace("$playername", p.getName())
				.replace("$ping", pingPlayer(p) + "")
				.replace("$hp", df.format(p.getHealth()))
				.replace("$maxhp", df.format(p.getHealthScale()))
				.replace("$food", df.format(p.getFoodLevel() + (double) p.getSaturation()))
				.replace("$bal", playerMoney + "")
				.replace("$entities", entities + "")
				.replace("$line-", new String(new char[linecounter]).replace("\0","§r") + new String(new char[length]).replace("\0","-"))
				.replace("$line=", new String(new char[linecounter]).replace("\0","§r") + new String(new char[length]).replace("\0","="))
				.replace("$randomcolor", "§" + colors[colorIteration % colors.length].getChar());
	}

	//	@EventHandler
	//	public void onBlockPlace(BlockPlaceEvent event) {
	//		Player p = event.getPlayer();
	//	}
	//	public static boolean deleteDirectory(String dirname) {
	//		File dir = new File(dirname);
	//		if (dir.isDirectory()) {
	//			File[] children = dir.listFiles();
	//			for (int i = 0; i < children.length; i++) {
	//				boolean success = deleteDirectory(children[i].getPath());
	//				if (!success) {
	//					return false;
	//				}
	//			}
	//		}
	//		System.out.println("Removing file or directory : " + dir.getName());
	//		return dir.delete();
	//	}
}
