package net.thearcanebrony.plugin.essentialtools.datastorage;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigDefaults {
	public static void setupDefaults(FileConfiguration c) {
		c.addDefault("# Use ALT+21 for color codes!", 0);
		c.addDefault("Chat Prefix", "§6[EssentialTools]§r ");
		c.addDefault("Welcome Message", "Welcome to AstroPlexMC! Enjoy your stay!");
		c.addDefault("RTP Range", 10000);
		c.addDefault("NoTrampleCrops", false);
		c.addDefault("Classic Mode", false);
		c.addDefault("Classic Liquids", false);
		c.addDefault("Obsidian Generators", true);
		c.addDefault("Grass Spreading", false);
		c.addDefault("Sidebar.Count", 10);
		c.addDefault("Sidebar.Header", "§6Astro§ePlex");

		c.addDefault("# For duplicate lines add §r in front", 0);
		c.addDefault(
				"# Variables (prefixed with $): players, maxplayers, tps, mem, playername, ping, hp, maxhp, food, bal",
				0);
		List<String> defaultSidebar = new ArrayList<String>();
		defaultSidebar.add("§7======================");
		defaultSidebar.add("§7Players online: §e$players§8/§6$maxplayers");
		defaultSidebar.add("§7TPS: §e$tps §7Mem: §e$mem%");
		defaultSidebar.add("§r§7======================");
		defaultSidebar.add("§7Player:   §6$playername");
		defaultSidebar.add("§7Ping:      §b$ping");
		defaultSidebar.add("§7Health:   §e$hp§7/§6$maxhp");
		defaultSidebar.add("§7Food:     §e$food§7/§620");
		defaultSidebar.add("§7Balance: §a$bal");
		defaultSidebar.add("§r§r§7======================");
		c.addDefault("Sidebar.Lines", defaultSidebar);
		List<String> defaultTablistHeader = new ArrayList<String>();
		defaultTablistHeader.add("§6Astro§ePlex");
		defaultTablistHeader.add("§7Online: §e$players§8/§6$maxplayers");
		defaultTablistHeader.add("§7Ping: §e$ping §7TPS: §e$tps");
		defaultTablistHeader.add("§7Mem: §e$mem% §7E: §e$entities");
		defaultTablistHeader.add("§7====================");
		c.addDefault("Tablist.Header", defaultTablistHeader);
		List<String> defaultTablistFooter = new ArrayList<String>();
		defaultTablistFooter.add("§7====================");
		defaultTablistFooter.add("§6$playername");
		defaultTablistFooter.add("§a$bal");
		defaultTablistFooter.add("§7Health: §e$hp§7/§6$maxhp");
		defaultTablistFooter.add("§7Food: §e$food§7/§620");
		c.addDefault("Tablist.Footer", defaultTablistFooter);
	}
}