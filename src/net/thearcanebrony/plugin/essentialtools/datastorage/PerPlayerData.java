package net.thearcanebrony.plugin.essentialtools.datastorage;

import java.util.HashMap;

import org.bukkit.Server;
import org.bukkit.entity.Player;

public class PerPlayerData {
	public Server server;

	public PerPlayerData(Server s) {
		server = s;
	}
public static HashMap<Player, PerPlayerData> playerData = new HashMap<Player, PerPlayerData>();
	
	public static PerPlayerData getPlayerData(Player p) {
		
		return playerData.getOrDefault(p, getDefaultPlayerData(p));
	}
    public static PerPlayerData getDefaultPlayerData(Player p) {
    	PerPlayerData dpd = new PerPlayerData(p.getServer());
		playerData.putIfAbsent(p, dpd);
		return playerData.getOrDefault(p, dpd);
	}
    public long timeSinceLastBreak = 0;
}